import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Observable, Subscription} from "rxjs";
import {error} from "@angular/compiler/src/util";
import { map, filter } from 'rxjs/operators'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  firstObsSubscription: Subscription
  secondObsSubscription: Subscription

  constructor() {
  }

  ngOnInit() {
    //every second new event will be emitted.
    // this.firstObsSubscription = interval(1000).subscribe(count => {
    //   console.log("count: ", count)
    // })
    console.log("init")
    //create method is deprecated
    const customIntervalObservable = Observable.create(observer => {
      let count: number = 0;
      console.log('second')
      setInterval(() => {
        // *** on unsubscribe console log doesnt stops.
        observer.next(count);
        // isn't incrementing--  count was defined in side set interval
        if(count === 3 ){
          observer.complete();
        }
        if(count >= 4){
          // both ways of sending error works
          observer.error("count  reached 3!!")
          //observer.error(new Error("count  reached 3!!"))
        }

        count = count + 1;
      }, 1000)
    })

    //OPERATORS - learnrxjs.io
    this.secondObsSubscription = customIntervalObservable.pipe(filter( (count: number) => {
        console.log("inside filter");
        return count > 0 ? true : false;
      }),
      map( (count: number) => {
        // Manipulated data
        return "Adding 3 to count: " +  (count + 3);
     })).subscribe(count => {
      console.log("count: ", count)
    }, error => {
      alert(error);
    }, ()=> console.log("Completed!!") );



    // error cancels the observable- no more event after that.
    // in case of error, complete never get called.
    // this.secondObsSubscription = customIntervalObservable.subscribe(count => {
    //   console.log("count: ", count)
    // }, error => {
    //   alert(error);
    // }, ()=> console.log("Completed!!") );
  }

  ngOnDestroy() {
    //observable dead here.
    // this.firstObsSubscription.unsubscribe();
    this.secondObsSubscription.unsubscribe();
  }

}
