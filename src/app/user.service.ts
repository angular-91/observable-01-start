import {EventEmitter, Injectable} from "@angular/core";
import {Subject} from "rxjs";

@Injectable({providedIn: "root"})
export class UserService {

  //event emiiter
  //activatedUser = new EventEmitter();


  //subject
  activatedUser =  new Subject<boolean>();

}
