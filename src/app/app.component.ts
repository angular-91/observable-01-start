import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from "./user.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  private userActivateSubscription: Subscription;

  constructor(private userService: UserService) {}

  userActivated = false;

  ngOnInit() {
    this.userActivateSubscription = this.userService.activatedUser.subscribe((didActivate)=>{
        this.userActivated = didActivate;
    })
  }

  ngOnDestroy() {
    this.userActivateSubscription.unsubscribe();
  }
}
